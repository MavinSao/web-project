

import java.util.*;

public class StarLoop {
    public static void main(String den[]){
        new StarLoop();
    }

    StarLoop(){
        Scanner sc=new Scanner(System.in);
        String ch,row;
        do{
            do{
                System.out.print("Input number of row :");
                row = sc.next();
            }while (!isValid(row));

            int num = Integer.parseInt(row);
            System.out.println("\n1 .Shape I\n");
            shapeOne(num);
            System.out.println("\n2 .Shape II\n");
            shapeTwo(num);
            System.out.println("\n3 .Shape III\n");
            shpaeThree(num);
            System.out.println("\n4 .Shape IV\n");
            shapeFour(num);
            System.out.println("\n5 .Shape V\n");
            shapeFive(num);
            System.out.println("\n6 .Shape VI\n");
            shpaeSix(num);

            System.out.println("\n------Press x|X to continue or any key to exit------\n");
            ch = sc.next();
        }while (ch.toLowerCase().equals("x"));
        System.out.println("Bye...");
    }

    void shapeOne(int n){
        for(int i=1;i<=n;i++){
            for(int j=0;j<n-i;j++){
                System.out.print(" ");
            }
            for(int k=0;k<2*i-1;k++){
                System.out.print("*");
            }
            System.out.println();
        }
    }
    void shapeTwo(int n){
        for(int i=n;i>0;i--){
            for(int j=0;j<n-i;j++){
                System.out.print(" ");
            }
            for(int k=2*i-1;k>0;k--){
                System.out.print("*");
            }

            System.out.println();
        }
    }
    void shpaeThree(int n){

        if(n%2==0)n++;
        for(int i=0;i<n;i++){
            if(i<=n/2){
                for(int j=0;j<n/2-i;j++){
                    System.out.print(" ");
                }
                for (int k=0;k<2*i+1;k++){
                    System.out.print("*");
                }
            }
            else {
                for(int j=0;j<i-n/2;j++){
                    System.out.print(" ");
                }
                for(int k=(2*n-2*i)-1;k>0;k--){
                    System.out.print("*");
                }
            }
            System.out.println();
        }

    }
    void shapeFour(int n){
        for(char i='A';i<'A'+n;i++){
            for(int j='A';j<=i;j++){
                System.out.print((char)j);
            }
            System.out.println();
        }
    }
    void shapeFive(int n){
        for(int i='A'+n;i>='A';i--){
            for(int j='A';j<i;j++){
                System.out.print((char)j);
            }
            System.out.println();
        }
    }
    void shpaeSix(int n){
        if(n%2!=0)n++;
        for(int i=0;i<n;i++){
            if(i<n/2){
                for(int j=n/2-i;j>0;j--){
                    System.out.print("* ");
                }
                for(int k=0;k<2*i;k++){
                    System.out.print("  ");
                }
                for(int l=n/2-i;l>0;l--) {
                    System.out.print("* ");
                }
            }else{
                for(int j=0;j<=i-n/2;j++){
                    System.out.print("* ");

                }
                for(int k=0;k<2*n-2*i-2;k++){
                    System.out.print("  ");
                }
                for(int l=0;l<=i-n/2;l++){
                    System.out.print("* ");
                }

            }
            System.out.println();
        }
    }
    boolean isValid(String row){
        try{
            int num = Integer.valueOf(row);
            if(num>7){
                return true;
            }else {
                System.out.println("Number of row must be greater than 7.");
                return false;
            }

        }catch (Exception e){
            System.out.println("Invalid number of row...!");
            return false;
        }
    }
}
